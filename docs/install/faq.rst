.. _install-faq:

FAQ
###

My board only has an SD card
============================

We boot the system using an SD card only.  However,  we need to merge firmware
and root file system images into a single image and store it into the SD card.
Luckily we provide a script for that:

.. code-block:: bash

        $ gunzip <firmware image>.wic.gz
        $ wget https://git.linaro.org/ci/job/configs.git/plain/ledge/ts/scripts/ts-merge-images.sh
        $ chmod +x ts-merge-images.sh
        $ ./ts-merge-images.sh <firmware image>.wic trs-image-trs-qemuarm64.wic

Verify the images are programmed correctly.  Note that only "ESP" and "Root Filesystem"
will be identical on your board.  The number and nature of the preceding are vendor specific.

Here is an example from a RockPI4b board:

.. code-block:: bash

        $ fdisk -l ts-firmware-rockpi4b.wic
        Disk ts-firmware-rockpi4b.wic: 2.28 GiB, 2443199488 bytes, 4771874 sectors
        Units: sectors of 1 * 512 = 512 bytes
        Sector size (logical/physical): 512 bytes / 512 bytes
        I/O size (minimum/optimal): 512 bytes / 512 bytes
        Disklabel type: gpt
        Disk identifier: B9476BE0-8456-4A3B-98D4-75A91739819F

        Device                     Start     End Sectors  Size Type
        ts-firmware-rockpi4b.wic1     64    8063    8000  3.9M unknown
        ts-firmware-rockpi4b.wic2   8064    8191     128   64K Microsoft basic data
        ts-firmware-rockpi4b.wic3   8192   16383    8192    4M Microsoft basic data
        ts-firmware-rockpi4b.wic4  16384   24575    8192    4M unknown
        ts-firmware-rockpi4b.wic5  24576   32767    8192    4M Microsoft basic data
        ts-firmware-rockpi4b.wic6  32768  557055  524288  256M EFI System           <------- ESP
        ts-firmware-rockpi4b.wic7 557056 4751359 4194304    2G Linux filesystem     <------- Root Filesystem

Q: How to increase OP-TEE core log level?
=========================================

Add ``CFG_TEE_CORE_LOG_LEVEL=3`` to ``EXTRA_OEMAKE`` in
``meta-ts/meta-arm/recipes-security/optee/optee-os.inc`` and rebuild (kas
build…)

Q: How to modify optee-os sources locally and rebuild?
======================================================

	#. Remove line ``INHERIT += rm_work`` in ``ci/base.yml``
	#. Run ``$ kas shell ci/rockpi4b.yml``
		#. ``bitbake -c cleansstate optee-os`` # WARNING removes source in work directory
		#. ``$ bitbake optee-os``
		#. Edit source files in ``build/tmp/work/rockpi4b-poky-linux/optee-os/<ver>/git``
		   ``$ bitbake -c compile -f optee-os`` # mandatory before kas build below it seems
	#. Exit kas shell and run ``$ kas build ci/rockpi4b.yml``

Q: Why is the internal eMMC not detected?
=========================================

Try a different USB-C power supply. We use a Dell one. I have another no-name PS
supposedly rated PD 100W which doesn’t work reliably.

Q: How to skip initramfs and boot to rootfs directly?
=====================================================

.. code-block:: bash

	$ efidebug boot add -b 1 TRS usb 0:1 Image -s 'panic=60 root=/dev/sda2 rootwait'; efidebug boot order 1; bootefi bootmgr

Q: On boot, the kernel logs warnings about GPT, how to fix them?
================================================================

They are harmless, they are caused by the fact that the actual device (USB key)
is larger than the image copied to it. The warnings can be removed by running
``gparted /dev/sdaX`` and accepting the prompt to fix the GPT info.

Q: On boot, the kernel logs “EXT4 … recovery complete”, what’s wrong?
=====================================================================

Usually harmless. The board was not powered off or rebooted cleanly. Use
``systemctl halt`` or ``systemctl reboot``.

Q: symbolize.py for TAs (on e.g., the fTPM TA) prints DWARF warnings and no source file/line info.
==================================================================================================

The default toolchains (``aarch64-linux-gnu-*``) is too old (7.2). Put a more
recent one in your ``PATH`` before invoking ``symbolize.py`` (Note: some source/file
line info are still missing, could be due to build flags)

Q: My board randomly hangs or crashes under system load.
========================================================

Some boards are very picky about their PSU.
Ensure you are using an official PSU. E.g for the RockPI4b
https://shop.allnetchina.cn/products/power-supply-adapter-qc-3-0-for-rock-pi-4

Do not use a 5v only USB-C PSU (such as a USB port on your laptop), as you will
hit random board stability issues.
