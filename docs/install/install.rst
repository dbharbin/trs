.. _getting_started:

Getting started
###############
The instructions on this page are a one time setup (per workspace). Two
installation/setup methods are provided below. First is the manual option. This
is for those who may want to integrate into their native development
environment. The second option is to create create a development environment in
docker. This will mean having Docker available on your development system.

.. _manual_installation:

Manual installation
###################

1. Install repo
***************
Note that here you don’t install a huge SDK, it’s simply a Python script that
you download and put in your ``$PATH``, that’s it. Exactly how to “install”
repo, can be found at the Google `repo`_ pages, so follow those instructions
before continuing.

2. Getting the source code
**************************
Now we will check out code for the TRS. This step is light weight and only check
out code necessary to build TRS. There are two flavors right now, either you
checkout the one tracking lastest on all gits or you'll checkout a certain
release (the difference is in the ``repo init`` line, as highlighted).

For latest, do this
===================

.. code-block:: bash
   :emphasize-lines: 3

    $ mkdir trs-workspace
    $ cd trs-workspace
    $ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git -m default.xml
    $ repo sync -j3

For a specific release, do this
===============================

.. code-block:: bash
   :emphasize-lines: 3

    $ mkdir trs-workspace
    $ cd trs-workspace
    $ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git -m default.xml -b <release-tag>
    $ repo sync -j3

.. _prerequisites:

3. Installing prerequisites
***************************
TRS depends on a couple of packages that needs to be present on the host system.
These are installed as distro packages and using Python pip.

Host packages
=============
.. tabs::

    .. tab:: Ubuntu / Debian

        **Host / apt packages**

        This will require your **sudo** password, from the root of the
        workspace:

        .. code-block:: bash

            $ cd <workspace root>
            $ make apt-prereqs

        This will install the following packages:

        .. literalinclude:: ../../scripts/apt-packages.txt
            :language: text


    .. tab:: Arch Linux

        .. warning::
            Just boiler plate, no complete instructions. Only Ubuntu versions
            tested so far.

        Install the necessary packages using pacman.

        ..
          [NEEDS_TO_BE_FIXED] - Add correct pre-req packages

        .. code-block:: bash

            $ sudo pacman -Syy
            $ sudo pacman -S git

    .. tab:: Fedora

        .. warning::
            Just boiler plate, no complete instructions. Only Ubuntu versions
            tested so far.

        Install the necessary packages using dnf.

        ..
          [NEEDS_TO_BE_FIXED] - Add correct pre-req packages

        .. code-block:: bash

            $ sudo dnf update
            $ sudo dnf install git

Python packages
===============

By default all python packages will be installed at ``<workspace
root>/.pyvenv`` using a virtual Python enviroment. The benefits by doing
so is that if we delete the ``.pyvenv`` folder, there will be no traces
left of the Python packages needed for TRS. It can eventually also avoid
clashing with tools needing other versions of some Python packages.

.. code-block:: bash

    $ cd <workspace root>
    $ make python-prereqs

4. Building
***********

4.1 Support virtualization with Xen (Optional)
==============================================

To support Xen in TRS, in the configuration file ``meta-trs/conf/distro/trs.conf``
you need to replace distro feature ``ewaol-baremetal`` with ``ewaol-virtualization``
and append to variable ``DISTRO_FEATURES``; with the virtualization feature, Xen
hypervisor and its associated packages (including kernel modules and tools) will
be built in TRS image.

.. code-block:: bash

    # In the file meta-trs/conf/distro/trs.conf
    DISTRO_FEATURES:append = " ewaol-virtualization"

4.2 Build firmwares and TRS image
=================================

Since we are using a virtual Python environment, we need begin by sourceing it.

.. code-block:: bash

    $ source <workspace root>/.pyvenv/bin/activate

.. note::
   The ``source`` command must be run **once** each time a new shell is created.

Next we start the build, this will probably take several hours on a normal
desktop computer the first time you're building it with nothing in the cache(s).
The TRS is based on various Yocto layers and if you don't have your ``DL_DIR``
and ``SSTATE_DIR`` set as an environment variable, those will be set to
``$HOME/yocto_cache`` by default. Note that the ``clean`` target does not remove
the download and sstate caches. ``make clean`` is a rather quick process that is
often needed after modifying the Yocto meta layers.

.. code-block:: bash

    $ cd <workspace root>
    $ make

After you complete the whole building process, if you want to only build
firmwares for saving time, you could use the command ``make meta-ts``; for only
building TRS image, the command ``make trs`` can be used.  You also can use the
command ``make trs-dev``, it builds TRS image with enabling `ewaol-sdk` distro
feature and includes debugging and profiling tools (e.g. gdb, perf, systemtap,
ltt-ng, etc).

5. Target specific installation
*******************************
After following the steps above, please continue with the target specific
instructions:

#. :ref:`install-qemu`
#. :ref:`install-other`

5. Tips and tricks
******************

5.1 Reference local mirrors
===========================
As the repo forest grows, the amount of time to run the initial ``repo sync``
increases. The repository tool is able to reference a locally cloned forest and
clone the bulk of the code from there, taking just the eventual delta between
local mirrors and upstream trees. The way to do this is to add the parameter
``--reference`` when running the ``repo init`` command, for example:

.. code-block:: bash

    $ repo init -u https://... --reference <path-to-my-existing-forest>

5.2 Local manifests
===================
In some cases we might want to use another remote, pick a certain commit or even
a add another repository to the current repo setup. The way to do that with
`repo`_ is to use `local manifests`_. The end result would be the same as
manually clone or checkout a certain tag or commit. The advantage of using a
local manifest is that when running "repo sync", the original manifest will not
override our temporary modifications. I.e., it's possible to reference and keep
using a temporary copy if needed.

.. _repo: https://source.android.com/docs/setup/develop#installing-repo
.. _local manifests: https://gerrit.googlesource.com/git-repo/+/master/docs/manifest-format.md#Local-Manifests


.. _docker_installation:

Docker Install
##############

This installation method has been created to aid developers in quickly setting
up an initial TRS development environment. By leveraging the scripts and
Dockerfile available in the `trs repository`_, with just a few steps you can
have a trs-development environment running in a docker container. The benefits
of using a container for your development environment include quickly
reproducing your environment, speed of setup, all devs in a similar
environment, can be customized/extended to meet your needs, usable across
different host platforms, and more.  

.. _trs repository: https://gitlab.com/Linaro/trusted-reference-stack/trs/-/tree/main/scripts/docker-scripts
 
Container Configuration
***********************
This section provides an overview of how this container is set up. 

.. image:: ../images/TRS-Figure-DockerMapping.png

Referring to the diagram above:

* The username is ``dev``
* When logging into the container, it defaults into the pre-determined ``$HOME/trs-workspace`` directory
* Under ``$HOME/trs-workspace`` is the ``./build`` directory that has a softlink to the ``$HOME/yocto_cache/`` directories
* This docker configuration provides three shared directories

  * The first, ``$HOME/trs_reference_repo`` on the Host is shared with ``$HOME/trs-reference-repo`` in the container. This allows a user to keep it updated from the host side and potentially be shared by multiple containers
  * The second and third directories are tied to the creation of a yocto build cache, also to reduce build times.  These default to ``$HOME/yocto_cache`` on the host and container. Two subdirectories are created under ``$HOME/yocto_cache``. These are ``$HOME/yocto_cache/sstate-cache`` and ``$HOME/yocto_cache/downloads``

* The default directories/shares described above may of course all be customized by modifying the Dockerfile and Scripts, but note that the naming must be assured to be consistent in all the files.

Tested Environments
*******************
The instructions/scripts in this section have been verified against Ubuntu
22.04 desktop machine and a share server environment also based on Ubuntu 20.04

Host Prerequisites
******************

- Assure that Docker has been installed on your Host development machine

.. code-block:: bash

    $: docker --version;
    Docker version 20.10.19, build d85ef84;

.. note::

   These instructions assume the user name is "dev"

Installation instructions
*************************

Since there are instructions for both the Host running Docker and the Container
that will have the Ubuntu 20.04 TRS development environment set up, the
following sections will delineate the difference by using "Host" or "Container"
in the header. That way a user will know where the commands are intended to
run.

1. Clone the TRS repository (**Host**)
======================================
Cloning the repo to be able to easily grab the scripts.

.. code-block:: bash

    $ cd ~
    $ mkdir trs-repo
    $ cd trs-repo
    $ git clone https://gitlab.com/Linaro/trusted-reference-stack/trs.git

Optionally check that the Dockerfile and scripts are present:

.. code-block:: bash

    $ ls ~/trs-repo/trs/scripts/docker-scripts
    Dockerfile  run-trs.sh  trs-install.sh

2. Build Docker Image (**Host**)
================================
Create a docker image the named "trs" 

.. code-block:: bash

    $ cd ~/trs-repo/trs/scripts/docker-scripts
    $ docker build -t trs .

.. note::

    The above defaults to a UID/GID of 1000/1000; typical of an Ubuntu Desktop.  If the host has a different UID/GID and it's desired for the container to have the same, use the following command instead of the one above:

    .. code-block:: bash

        $ cd ~/trs-repo/trs/scripts/docker-scripts
        $ docker build --build-arg USER_UID=$(id -u) --build-arg USER_GID=$(id -g) -t trs .
           

.. hint::

    During a docker build, it's not uncommon to see warnings such as the following that can be ignored.

    For example 

    .. code-block:: bash
        
        WARNING: apt does not have a stable CLI interface. Use with caution in scripts.
        
Optionally, after completion of the docker build, you can confirm that the
images are there and look OK. Assuming you had no other docker images, you
should see something similar to the following:

.. code-block:: bash

    $ docker images
    REPOSITORY   TAG       IMAGE ID         CREATED            SIZE
    trs          latest    2a10a95eacd2   10 seconds ago   336MB
    ubuntu       22.04     a8780b506fa4   4 weeks ago      77.8MB

3. Download and sync the TRS source using Repo tool (**Host**)
==============================================================
As described above, the Host and Container share the TRS repo in a shared
directory. This section sets up this share TRS repo with the following
commands.

.. code-block:: bash

    $ cd ~
    $ mkdir trs_reference_repo
    $ cd trs_reference_repo
    $ repo init -u https://gitlab.com/Linaro/trusted-reference-stack/trs-manifest.git -m default.xml
    $ repo sync 

With all the above steps completed, we're now ready to launch the TRS container!

.. warning:: 

    The location above is important as this is a shared folder between the Host and the Container. If the user chooses to   change this location, the scripts/Dockerfile must be updated to align.

4. Create and enter the Container (**Host**)
============================================
The following commands will launch the container using the Dockerfile built in
the earlier steps

.. code-block:: bash

    $ cd ~/trs-repo/trs/scripts/docker-scripts
    $ docker build -t trs .
    $ ./run-trs.sh

    dev@2d0b8419dac3:~/trs-workspace$

A new prompt will be shown in your terminal similar to the above and you're now
working in the docker container!

Optionally, **from the Container**, some quick checks can be executed to assure
that the container is set up right. This includes assuring all the shares have
permissions set correctly, and that the build directory is linked to the
yocto_cache directory using a soft link. 

.. code-block:: bash

    dev@92fae72fafee:~/trs-workspace$ ls -l
    total 8
    drwxr-xr-x 1 dev dev 4096 Jan 27 21:22 build
    -rwxrwxr-x 1 dev dev 1936 Jan 27 20:41 trs-install.sh
    dev@92fae72fafee:
    
    dev@2d0b8419dac3:~/trs-workspace$ ls -l build
    total 0
    lrwxrwxrwx  1 dev dev    31 Jan 27 21:22 downloads -> /home/dev/yocto_cache/downloads
    lrwxrwxrwx  1 dev dev    34 Jan 27 21:22 sstate-cache -> /home/dev/yocto_cache/sstate-caches
    dev@92fae72fafee:

    dev@2d0b8419dac3:~/trs-workspace$ ls -l ~
    total 16
    drwxrwxr-x 17 dev  dev  4096 Jan 19 23:26 trs-reference-repo
    drwxr-xr-x  1 dev  dev  4096 Jan 27 21:27 trs-workspace
    drwxr-xr-x  1 root root 4096 Jan 27 21:21 yocto_cache

    dev@92fae72fafee:~/trs-workspace$ ls ~/yocto_cache -l
    total 80
    drwxrwxr-x   4 dev dev 73728 Jan 27 22:05 downloads
    drwxrwxr-x 259 dev dev  4096 Jan 27 21:28 sstate-cache

    dev@2d0b8419dac3:~/trs-workspace$

    dev@92fae72fafee:~/trs-workspace$ ping google.com
    PING google.com (142.250.188.238) 56(84) bytes of data.
    64 bytes from lax31s15-in-f14.1e100.net (142.250.188.238): icmp_seq=1 ttl=116 time=31.7 ms
    64 bytes from lax31s15-in-f14.1e100.net (142.250.188.238): icmp_seq=2 ttl=116 time=29.2 ms
    64 bytes from lax31s15-in-f14.1e100.net (142.250.188.238): icmp_seq=3 ttl=116 time=26.4 ms
    ^C
    --- google.com ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 2001ms
    rtt min/avg/max/mdev = 26.419/29.119/31.708/2.160 ms
    dev@92fae72fafee:~/trs-workspace$ ^C
    dev@92fae72fafee:~/trs-workspace$ 

If the user, group and shares all look good and a ping verified we have
connectivity to the internet, then we're ready to move on to the final step,
which is performing a trs build!

6 Build TRS (**Container**) =============================== To verify
everything is correct, perform a build by executing the ``./trs-install.sh -h
-r`` command. Be sure to include the ``-h -r`` options when kicking off the
build script for the build to work correctly. 

.. code-block::

    dev@92fae72fafee:~/trs-workspace$ ./trs-install.sh -h -r
    Using Yocto cache from host
    Using reference from host
    Downloading Repo source from https://gerrit.googlesource.com/git-repo
    repo: Updating release signing keys to keyset ver 2.3
    warning: gpg (GnuPG) is not available.
    warning: Installing it is strongly encouraged.
      
    repo has been initialized in /home/dev/trs-workspace
    Fetching: 71% (10/14) Linaro/trusted-reference-stack/trs.git
    ...
    
.. note::
    
    This build currently requires several hours to complete. There will be a number of warnings during the build, but this is OK. If completes successfully, then you'll see a message prior to returning to the prompt similar to the following:

    .. code-block::

        Summary: There were 4 WARNING messages.
        Build succeeded, see output in build/tmp_trs-qemuarm64/deploy directories.
        dev@92fae72fafee:~/trs-workspace$

Once the build succeeds, the user can perform a final verification step, which
is to execute the steps in the :ref:`install-qemu` section of this document.

