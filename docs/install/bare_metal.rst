.. _install-other:

Run on bare-metal
#################

This document describes how to run TRS for various supported targets.

The easiest way to get TRS up and running is

* Flash your device firmware to the correct medium
* Prepare a USB disk with the OS

Flashing the firmware
=====================

Firmware is device specific.  As a result each of the supported boards has
vendor specific requirements for writing the firmware.

You can find per device instructions in our :ref:`Installing firmware` section.

.. warning::

    If your firmware is going to be flashed on an SD card make sure the device
    in ``/dev`` is present before proceeding.  If the ``/dev/sdX`` file is missing
    you will end up creating a static in ``/dev`` and write nothing on the SD
    card.  The card will not be detected until you delete the file!

Prepare USB stick with TRS
==========================

To flash the rootfs image you built above, from your TRS build directory

.. code-block:: bash

	$ sudo dd if=build/tmp_trs-qemuarm64/deploy/images/trs-qemuarm64/trs-image-trs-qemuarm64.wic of=/dev/sdX bs=1M status=progress
	$ sync

Boot TRS
========

Attach the USB stick on USB port and reset the device. If your USB stick is
detected TRS will boot automatically.

.. warning::

    Always prefer USB 3.0+ ports. If you have problems booting TRS, interrupt
    U-Boot boot sequence and make sure your disk is detected.

.. code-block:: bash

        => usb start
        => usb storage
          Device 0: Vendor: SanDisk Rev: 1.00 Prod: Cruzer Blade
                    Type: Removable Hard Disk
                    Capacity: 29340.0 MB = 28.6 GB (60088320 x 512)
