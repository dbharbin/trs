################
Running a distro
################

Since the firmware provides a [UEFI]_ interface you are free to choose the
distro you prefer. However boards that embed the UEFI keys in the U-Boot binary
:ref:`Hardware and UEFI variable limitations` will only be able to boot signed
binaries.  Look at :ref:`Building with your own certificates` if you want to
build and your own vertical distro and sign your binaries.  If you use the
pre-compiled firmware binaries you can test that with our own TRS distro.

Download TRS
************

Download a .wic.gz image from `here <https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/trs-image-trs-qemuarm64.rootfs.wic.gz?job=build-meta-trs>`_

Running TRS
***********

Throughout the examples we will be using a USB disk.  If you prefer a different
installation medium you need to adjust the commands accordingly.

You can prepare one with

.. code-block:: bash

    zcat trs-image-trs-qemuarm64.rootfs.wic.gz > /dev/sdX

TRS comes with GRUB installed.  As a result there is nothing else you have to do
to boot your board.  Just insert your USB disk and your device will automatically
boot.

.. note::

   TRS, on the first boot, will automatically encrypt your root filesystem if
   measured boot is enabled on your firmware.

Running TRS without GRUB
************************

If you want to skip GRUB you need to configure the EFI boot manager properly.

Run on QEMU arm64
=================

QEMU can provide a TPM implementation via `Software TPM <https://github.com/stefanberger/swtpm>`_

[SWTPM]_ provides a memory mapped device which adheres to the
`TCG TPM Interface Specification <https://trustedcomputinggroup.org/wp-content/uploads/TCG_PCClientTPMInterfaceSpecification_TIS__1-3_27_03212013.pdf>`_

.. code-block:: bash

    sudo apt install swtpm swtpm-tools

    mkdir /tmp/mytpm1 -p

    swtpm_setup --tpmstate /tmp/mytpm1 --tpm2 --pcr-banks sha256
    swtpm socket --tpmstate dir=/tmp/mytpm1 \
        --ctrl type=unixio,path=/tmp/mytpm1/swtpm-sock \
        --log level=0 --tpm2 -t -d

.. code-block:: bash

    gunzip trs-image-trs-qemuarm64.rootfs.wic.gz
    qemu-system-aarch64 -m 2048 -smp 2 -nographic -cpu cortex-a57 \
        -bios flash.bin -machine virt,secure=on \
        -drive id=os,if=none,file=trs-image-trs-qemuarm64.rootfs.wic \
        -device virtio-blk-device,drive=os \
        -chardev socket,id=chrtpm,path=/tmp/mytpm1/swtpm-sock \
        -tpmdev emulator,id=tpm0,chardev=chrtpm \
        -device tpm-tis-device,tpmdev=tpm0

.. code-block:: bash

    => efidebug boot add -b 1 TRS virtio 0:1 Image -i virtio 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr


Run on SynQuacer
================

.. code-block:: bash

    => efidebug boot add -b 1 TRS usb 0:1 Image -i usb 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr

Run on stm32mp157c dk2 or ev1
=============================

TRS does not yet provice Armv7 builds.  Command for reference

.. code-block:: bash

    => efidebug boot add -b 1 TRS usb 0:1 Image -i usb 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr

run on rockpi4b
===============

.. code-block:: bash

    => efidebug boot add -b 1 TRS usb 0:1 Image -i usb 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr

Run on Raspberry Pi4
====================

.. code-block:: bash

    => efidebug boot add -b 1 TRS usb 0:1 Image -i usb 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr

Run on Xilinx KV260 AI Starter and Commercial kit
=================================================

USB is not yet supported in the kernel.  Use the mmc interface instead

.. code-block:: bash

    => efidebug boot add -b 1 TRS mmc 0:1 Image -i mmc 0:1 ledge-initramfs.rootfs.cpio.gz -s 'root=UUID=6091b3a4-ce08-3020-93a6-f755a22ef03b rootwait panic=60'
    => efidebug boot order 1
    => bootefi bootmgr
