# Copyright (c) 2023, Linaro Limited.
#
# SPDX-License-Identifier: MIT

# Require image recipe for ewaol-baremetal DISTRO_FEATURE
require ${@bb.utils.contains('DISTRO_FEATURES','ewaol-baremetal',\
'${EWAOL_DISTRO_LAYERDIR}/recipes-core/images/ewaol-baremetal-image.bb', '', d)}

# Require image recipe for ewaol-virtualization DISTRO_FEATURE
require ${@bb.utils.contains('DISTRO_FEATURES','ewaol-virtualization',\
'${EWAOL_DISTRO_LAYERDIR}/recipes-core/images/ewaol-virtualization-image.bb', '', d)}

# Require image recipe for ewaol-sdk DISTRO_FEATURE
require ${@bb.utils.contains('DISTRO_FEATURES','ewaol-sdk',\
'${EWAOL_DISTRO_LAYERDIR}/conf/distro/include/ewaol-sdk.inc', '', d)}

# guest config has multiconfig issues currently, remove as workaround
IMAGE_INSTALL:remove = "ewaol-guest-vm-package"

# ewaol-guest-vm-package.bb sets multi-config dependency, but trs doesn't do
# multi-config building; so cleanup multi-config dependency.
do_image_wic[mcdepends] = ""

# Override ewaol's function for calculation vms rootfs size
python compute_summed_guest_vms_rootfs_sizes() {
}
